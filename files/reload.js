(function() {
	"use script";
	function dorequest() {
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			if (xhr.readyState == XMLHttpRequest.DONE) {
				if (xhr.status != 200) {
					setTimeout(dorequest, 500);
				} else {
					var text = xhr.response;
					if (text == "restart") {
						dorequest();
					} else {
						location.reload();
					}
				}
			}
		}
		xhr.open("GET", "/reload-api/query")
		xhr.send()
	}
	dorequest()
})();

