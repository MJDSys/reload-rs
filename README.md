reload-rs
=========

reload-rs provides a simple web server for static websites that will automatically reload any page whenever a file in the web server root directory changes.

To use reload-rs, first install it with cargo by running ```cargo install reload-rs```.  Then run ```reload-rs``` in the directory you want to serve your files from.  Run ```reload-rs --help``` for more options.

Once reload-rs has started, visit http://localhost:8080 in your web browser to view your web site.
