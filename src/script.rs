// Copyright 2016-2017 Matthew Dawson <matthew@mjdsystems.ca
// This file is part of reload-rs.
//
// reload-rs is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reload-rs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reload-rs.  If not, see <http://www.gnu.org/licenses/>.

use iron::status;
use iron::prelude::*;

const SCRIPT: &'static str = include_str!("../files/reload.js");

pub fn serve(_: &mut Request) -> IronResult<Response> {
    Ok(Response::with((status::Ok, SCRIPT)))
}
