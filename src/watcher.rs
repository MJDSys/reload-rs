// Copyright 2016-2017 Matthew Dawson <matthew@mjdsystems.ca
// This file is part of reload-rs.
//
// reload-rs is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reload-rs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reload-rs.  If not, see <http://www.gnu.org/licenses/>.

use std::sync::{Arc, Mutex};
use std::sync::mpsc::{channel, sync_channel, SyncSender};
use std::thread;
use std::time::Duration;

use bus::Bus;

use iron::prelude::*;
use iron::middleware::Handler;
use iron::status::Status;

use notify::{RecommendedWatcher, RecursiveMode, Watcher};

pub struct WatcherHandler {
    bus: Arc<Mutex<Bus<bool>>>,
    start: Arc<Mutex<SyncSender<()>>>,
}

impl Handler for WatcherHandler {
    fn handle(&self, _: &mut Request) -> IronResult<Response> {
        let response = {
            let mut rx_bus = {
                let mut bus = self.bus.lock().unwrap();
                let bus = bus.add_rx();
                self.start.lock().unwrap().try_send(()).unwrap_or(());
                bus
            };
            if rx_bus.recv().unwrap() {
                "reload"
            } else {
                "restart"
            }
        };
        Ok(Response::with((Status::Ok, response)))
    }
}

pub fn setup_watcher(folder: &str) -> WatcherHandler {
    let (tx, rx) = channel();
    let (sync_tx, sync_rx) = sync_channel(1);

    let bus = Arc::new(Mutex::new(Bus::new(10)));

    let tx_bus = bus.clone();
    let folder = folder.to_string();
    thread::spawn(move || {
        let mut watcher: RecommendedWatcher = Watcher::new_raw(tx).unwrap();

        watcher.watch(folder, RecursiveMode::Recursive).unwrap();

        let bus = tx_bus;
        loop {
            sync_rx.recv().unwrap();
            match rx.recv_timeout(Duration::from_secs(5)) {
                Ok(e) => {
                    bus.lock().unwrap().broadcast(true);
                    println!("{:?}", e);
                    while let Ok(_) = rx.recv_timeout(Duration::from_millis(50)) {}
                }
                Err(_) => bus.lock().unwrap().broadcast(false),
            };
        }
    });

    WatcherHandler {
        bus: bus,
        start: Arc::new(Mutex::new(sync_tx)),
    }
}
