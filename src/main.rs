// Copyright 2016-2017 Matthew Dawson <matthew@mjdsystems.ca
// This file is part of reload-rs.
//
// reload-rs is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// reload-rs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with reload-rs.  If not, see <http://www.gnu.org/licenses/>.

extern crate bus;
#[macro_use]
extern crate clap;
extern crate iron;
extern crate staticfile;
extern crate mount;
extern crate notify;

mod watcher;
mod script;

use std::io;
use std::io::prelude::*;
use std::path::Path;

use clap::Arg;

use iron::headers::ContentLength;
use iron::prelude::*;
use iron::response::{ResponseBody, WriteBody};
use mount::Mount;
use staticfile::Static;

const SCRIPT_INCLUDE: &'static str = "\n<script type=\"text/javascript\" \
                                      src=\"/script-reload-rs/reload.js\"></script>\n";

struct WriteBodyAppender(Option<Box<WriteBody>>);

impl WriteBody for WriteBodyAppender {
    fn write_body(&mut self, response: &mut ResponseBody) -> io::Result<()> {
        try!(self.0.as_mut().unwrap().write_body(response));
        try!(write!(response, "{}", SCRIPT_INCLUDE));
        Ok(())
    }
}

fn append_script_to_html(request: &mut Request, mut response: Response) -> IronResult<Response> {
    if let Some(segments) = request.url.clone().into_generic_url().path_segments() {
        if segments.last().map(|x| x == "" || x.ends_with(".html")).unwrap_or(false) {
            if let Some(old_length) = response.headers.get::<ContentLength>().cloned() {
                response.headers
                    .set(ContentLength(old_length.checked_add(SCRIPT_INCLUDE.len() as u64)
                        .unwrap()));
            }
            response.body = Some(Box::new(WriteBodyAppender(response.body)));
            return Ok(response);
        }
    }
    Ok(response)

}


fn main() {
    let matches = app_from_crate!()
        .arg(Arg::with_name("listen")
            .help("Address to serve files on")
            .short("l")
            .long("listen")
            .value_name("LISTEN ADDRESS")
            .default_value("localhost:8080")
            .empty_values(false))
        .arg(Arg::with_name("folder")
            .help("Folder to serve files from and to watch")
            .short("f")
            .long("folder")
            .value_name("ROOT")
            .default_value("./")
            .empty_values(false))
        .get_matches();

    let folder = matches.value_of("folder").unwrap();
    let mut router = Mount::new();
    let mut chain = Chain::new(Static::new(Path::new(folder)));
    chain.link_after(append_script_to_html);

    router.mount("/", chain)
        .mount("/reload-api/", watcher::setup_watcher(folder))
        .mount("/script-reload-rs/reload.js", script::serve);
    Iron::new(router).http(matches.value_of("listen").unwrap()).unwrap();
}
